import collections
import math
import re
import sys


def contar_palavras(arquivo):
    palavras = collections.defaultdict(int)
    # abrir o arquivo para ler
    contagem = open(arquivo, 'r')
    # ler o conteudo por linha e separar as palavras
    for linha in contagem:
        palavra = re.findall("\w+", linha)
        for word in palavra:
            # transforma todas as palavras em lower-case
            word = word.lower()
            if len(word) > 2:
                palavras[word] += 1

    contagem.close()
    return palavras


def freq(N, arquivo):
    palavras = contar_palavras(arquivo)
    palavras_por_contagem = sorted(
        palavras.items(), key=lambda item: item[1], reverse=True)

    # mostrar os primeiros N itens de palavras_por_contagem
    for palavra in palavras_por_contagem[:N]:
        print(palavra[0], palavra[1])


def freq_word(palavra, arquivo):
    if len(palavra) < 2:
        print("A palavra precisa ter ao menos duas letras!")
    elif palavra.isalpha():
        palavras = contar_palavras(arquivo)
        print("Ocorrências:", palavras[palavra.lower()])
    else:
        print("O termo digitado deve ser uma palavra. Números e pontuação são desconsiderados!")


def search(termo, arquivos):
    termos = re.findall("\w+", termo)
    tfidf = collections.defaultdict(lambda: 0.0)

    for termo_busca in termos:
        term_frequency = collections.defaultdict(lambda: 0.0)

        for arquivo in arquivos:
            palavras = contar_palavras(arquivo)

            total_termo = palavras[termo_busca.lower()]
            total_palavras = sum(palavras.values())

            if total_termo:
                term_frequency[arquivo] = total_termo / total_palavras

        numero_docs = len(arquivos)
        termo_doc = len(term_frequency)

        if not termo_doc:
            print(f'O termo {termo} não foi encontrado nos documentos')
            return

        inverse_document_frequency = math.log(numero_docs / termo_doc, 10)

        for arquivo in arquivos:
            tfidf[arquivo] += term_frequency[arquivo] * \
                inverse_document_frequency

    numero_termos = len(termos)

    for arquivo in arquivos:
        tfidf_final = tfidf[arquivo] / numero_termos
        print(arquivo, tfidf_final)


if __name__ == '__main__':
    argc = len(sys.argv)

    if argc == 4 and sys.argv[1] == '--freq':
        freq(int(sys.argv[2]), sys.argv[3])
    elif argc == 4 and sys.argv[1] == '--freq-word':
        freq_word(sys.argv[2], sys.argv[3])
    elif argc >= 4 and sys.argv[1] == '--search':
        search(sys.argv[2], sys.argv[3::])
    else:
        print("""NOME
    indexer - indexa palavras de documentos de texto

SINOPSE
    indexer --freq N ARQUIVO
    indexer --freq-word PALAVRA ARQUIVO
    indexer --search TERMO ARQUIVO [ARQUIVO ...]
""")

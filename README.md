# Indexer

Tecnologia em Análise e Desenvolvimento de Sistemas
Setor de Educação Profissional e Tecnológica - SEPT
Universidade Federal do Paraná - UFPR

---

_DS143 - Estruturas de dados II_

Marina Neves Beppler - GRR20211593

# Documentação do trabalho prático


## Primeiros passos

Como exigido, o trabalho prático envolveu a criação de um programa _indexer_ que fizesse contagem e indexação de documentos de texto.

Para a execução do programa _Indexer_, o único requerimento e possuir [Python 3](https://www.python.org/downloads/) instalado na máquina em que se pretende executá-lo. Utilizando o Linux (ou Windows PowerShell), ele pode ser executado por meio de `./indexer`, juntamente com um de seus três comandos `--freq`, `--freq-word` ou `--search`.

## Descrição

_Indexer_, o programa em questão, tem como objetivo indexar palavras de um ou mais documentos de texto. Ele realiza a contagem das palavras no texto, permitindo que essas sejam ordenadas e vistas por frequência. Com a contagem, também permite a busca de uma palavra específica no documento e uma seleção de documentos relevantes de acordo com o termo desejado.

Para que tal contagem, busca e comparação ocorra corretamente, o programa converte todas as letras para minúsculas, também desconsiderando caracteres como números e pontuações.Quando executado com a opção `--freq`, o programa _indexer_ exibe o número de ocorrências das **N** palavras mais frequentes no documento passado como parâmetro, em ordem decrescente de ocorrências. Já quendo executado com a opção `--freq-word`, o programa exibe a contagem de ocorrências de um **termo** em um **arquivo** especificado. Por fim, `--search` exibe uma lista em ordem decrescente dos **arquivos** mais relevantes em uma pesquisa por **termo**. O **termo** pode conter mais de uma palavra, e nesses caso, deve estar entre áspas.

O programa funciona via linha de comando e necessita que os arquivos de texto a serem analisados estejam no mesmo diretório que os arquivos do programa para que funcione corretamente.

## Uso

`--freq N ARQUIVO`

Exibe o número de ocorrência das N palavras que mais aparecem em ARQUIVO, em ordem decrescente de ocorrência.

`--freq-word PALAVRA ARQUIVO`

Exibe o número de ocorrências de PALAVRA em ARQUIVO.

`--search TERMO ARQUIVO [ARQUIVO ...]`

Exibe uma listagem dos ARQUIVOS mais relevantes encontrados pela busca por TERMO. A listagem é apresentada em ordem descrescente de relevância. TERMO pode conter mais de uma palavra. Neste caso, deve ser indicado entre àspas.


## indexer

É um shell script que permite que a execução do programa seja o mais simples possível, ocorrendo apenas pelo nome "indexer", sem depender de específicar uma linguagem específica — nesse caso, Python.

## indexer.py

É o programa em si, a CLI que realmente iremos usar ao executar _indexer_. Usando o modulo `sys` do Python, são analisados os argumentos adicionados juntamente a cada execução do programa em linha de comando para definir a opção escolhida, além dos parâmetros necessários para seu funcionamento em si. Essa análise é feita na função _main_ de indexer, e cada opção escolhida depende de duas funções definidas em _index.py_: `contar_palavras(arquivo)`, do qual dependem as execução de todas as outras e da função associada a opção escolhida.

### contar_palavras(arquivo)

Função da qual dependem todas as outras. Ela abre o arquivo desejado para leitura, lê seu conteuúdo por linhas, separa as palavras existentes (desconsiderando quaisquiser símbolos ou números que estejam no meio do texto) e transforma as palavras encontradas em lowercase. Fazendo uma conferência do tamanho das palavras (e desconsiderando todas aquelas com menos de duas letras), faz a contagem de cada palavra, armazenando a palavra em si e o seu número de ocorrências em um dicionário.

### freq

Função que atende ao comando `--freq`. Utiliza da função `contar_palavras()`, armazenando seu resultado em dicionário e então ordena os dados desse dicionário em ordem decrescente. São mostrados os primeiros N itens (incluíndo nome e frequência) dessa ordenação conforme "N" definido pelo usuário.

### freq-word

Confere se o termo para busca definido pelo usuário é maior do que dois caracteres e se é realmente formado por letras, não números ou caractéres especiais (por uso da função `isalpha()`); Se o termo inserido se enquadra nesses critérios, a contagem das ocorrências é realizada pela função `contar_palavras()` e o resultado referente ao termo específico buscado é imprimido. 

### search

Nessa função, é permitida a busca por mais de um termo por meio do uso de áspas, além da busca entre vários arquivos. Primeiro, também fazendo uso da função `contar_palavras()`, é definido a _term frequency_(TF) de acordo com a seguinte fórmula:

```
TF(t,d) = (Número de vezes que t aparece em d) / (Total de palavras em d)
```
É calculado o número de vezes que `t` aparece no documento, dividido pelo número total de palavras no documento:

Depois, é definida a _inverse document frequency_(IDF), de acordo com a seguinte fórmula:

```
IDF(t,D) = log[ (Número de Documentos) / (Número de documentos em que t está presente) ]
```

No qual o logarítmo é de base 10. É a frequência inversa de documento para um conjunto de documentos `D` é o logaritmo da divisão do número total de documentos pelo número de documentos que contém o termo `t` em questão:

Finalmente, é calculado o TFIDF, que é a multiplicação dos resultados das contas anteriores. Ela indica qual a relevância de um termo `t` para um documento `d` dentre uma coleção de documentos `D`:

```
TFIDF(t,d,D) = TF(t,d) * IDF(t,D)
```

Caso mais de um termo tenha sido inserido — por meio do uso de áspas — é feita a média entre TFIDF de cada uma das palavras inseridas. Isso é feito para cada arquivo analisado. Por fim, é impresso o "nome" do arquivo e sua relevância, em ordem decrescente de relevância para o(s) termo(s) pesquisados.